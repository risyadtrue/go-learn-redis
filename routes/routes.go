package routes

import (
	"fmt"
	"github.com/go-chi/chi"
	"log"
	"net/http"
	"redis-golang/controller"
)

func CollectionRoutes() {
	r := chi.NewRouter()
	r.Get("/welcome", controller.WelcomePage)
	r.Post("/user", controller.CreateUser)
	r.Get("/user", controller.ListUser)
	r.Post("/redis/hm", controller.SetRedisHM)
	r.Get("/redis/hm/{field}", controller.GetRedisHM)
	r.Get("/redis/hgetall", controller.GetRedisHGetAll)
	r.Delete("/redis/hdell/{field}", controller.RedisRedisHDel)
	r.Delete("/redis/del", controller.RedisRedisDel)

	fmt.Println("Running in port localhost:10000")
	log.Fatal(http.ListenAndServe(":10000", r))
}
