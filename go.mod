module redis-golang

go 1.17

require (
	github.com/go-chi/chi v1.5.4
	github.com/go-redis/redis v6.15.9+incompatible
	github.com/lib/pq v1.10.4
)

require (
	github.com/onsi/ginkgo v1.16.5 // indirect
	github.com/onsi/gomega v1.18.1 // indirect
)
