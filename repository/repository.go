package repository

import (
	"redis-golang/connection"
	"redis-golang/controller/dto"
)

func CreateUser(data dto.RequestUser) (userId int, err error) {
	conn, err := connection.PostgresConn()
	if err != nil {
		return
	}

	var id int
	query := "INSERT INTO users (username ,email, fullname, age) VALUES ($1, $2, $3, $4) RETURNING user_id"
	err = conn.QueryRow(query, data.Username, data.Email, data.Fullname, data.Age).Scan(&id)

	return
}

func GetUser() (listUser []dto.ResponseUser, err error) {
	conn, err := connection.PostgresConn()
	if err != nil {
		return
	}

	query := "SELECT user_id, username ,email, fullname, age FROM users"
	rows, err := conn.Query(query)
	if err != nil {
		return
	}
	defer rows.Close()

	for rows.Next() {
		user := dto.ResponseUser{}
		rows.Scan(&user.UserId, &user.Username, &user.Email, &user.Fullname, &user.Age)
		listUser = append(listUser, user)
	}

	return
}
