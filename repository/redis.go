package repository

import (
	"context"
	"encoding/json"
	"fmt"
	"redis-golang/connection"
	"time"
)

func RedisSet(ctx context.Context, keys string, data interface{}, expired time.Duration) (err error) {
	conn := connection.RedisConn()
	dataBytes, err := json.Marshal(data)
	if err != nil {
		fmt.Println("bytes", err.Error())
		return
	}

	err = conn.Set(keys, string(dataBytes), expired).Err()
	if err != nil {
		fmt.Println("set", err.Error())
		return err
	}

	return
}

func RedisGet(ctx context.Context, keys string, data interface{}) (err error) {
	conn := connection.RedisConn()

	redisData, err := conn.Get(keys).Result()
	if err != nil {
		fmt.Println("set", err.Error())
		return err
	}

	err = json.Unmarshal([]byte(redisData), &data)
	if err != nil {
		return err
	}

	return
}

func RedisHMSet(keys string, data map[string]interface{}, exp time.Duration) (err error) {
	conn := connection.RedisConn()
	err = conn.HMSet(keys, data).Err()
	if err != nil {
		return
	}

	conn.Expire(keys, exp)
	return
}

func RedisHMGet(keys string, field string, data interface{}) (err error) {
	conn := connection.RedisConn()
	// HGet spesifik value
	result, err := conn.HGet(keys, field).Result()
	if err != nil {
		return
	}

	err = json.Unmarshal([]byte(result), &data)

	// fungsi sama tapi beda return

	//result, err := conn.HMGet(keys, field).Result()
	//if err != nil {
	//	return
	//}
	//
	//err = json.Unmarshal([]byte(result[0].(string)), &data)
	return
}

func RedisHGetAll(keys string) (res map[string]string, err error) {
	conn := connection.RedisConn()
	res, err = conn.HGetAll(keys).Result()

	return
}

func RedisHDel(keys string, field ...string) (err error) {
	conn := connection.RedisConn()
	err = conn.HDel(keys, field...).Err()
	return
}

func RedisDel(keys ...string) (err error) {
	conn := connection.RedisConn()
	err = conn.Del(keys...).Err()

	return
}
