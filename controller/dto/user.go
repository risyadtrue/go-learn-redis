package dto

type RequestUser struct {
	Username string `json:"username"`
	Email    string `json:"email"`
	Fullname string `json:"fullname"`
	Age      int    `json:"age"`
}

type ResponseUser struct {
	UserId   int    `json:"user_id"`
	Username string `json:"username"`
	Email    string `json:"email"`
	Fullname string `json:"fullname"`
	Age      int    `json:"age"`
}

type RequestJenisMakanan struct {
	Makanan []ListMakanan `json:"makanan"`
}

type ListMakanan struct {
	NamaMakanan  string `json:"nama_makanan"`
	TypeMakanan  string `json:"type_makanan"`
	HargaMakanan int    `json:"harga_makanan"`
	PorsiMakanan string `json:"porsi_makanan"`
}
