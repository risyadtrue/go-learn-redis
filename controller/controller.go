package controller

import (
	"encoding/json"
	"fmt"
	"github.com/go-chi/chi"
	"github.com/go-redis/redis"
	"net/http"
	"redis-golang/controller/dto"
	"redis-golang/helper"
	"redis-golang/repository"
	"time"
)

func WelcomePage(writer http.ResponseWriter, request *http.Request) {
	helper.HttpResponseSuccess(writer, request, "Hello World!")
}

func CreateUser(writer http.ResponseWriter, request *http.Request) {
	var dataUser dto.RequestUser
	err := json.NewDecoder(request.Body).Decode(&dataUser)
	if err != nil {
		helper.HttpResponseError(writer, request, err.Error(), http.StatusBadRequest)
		return
	}

	userId, err := repository.CreateUser(dataUser)
	if err != nil {
		helper.HttpResponseError(writer, request, err.Error(), http.StatusInternalServerError)
		return
	}

	// get data user from data
	keys := "list-user-data"
	var listUser []dto.ResponseUser
	err = repository.RedisGet(request.Context(), keys, &listUser)
	if err != nil {
		if err != redis.Nil {
			helper.HttpResponseError(writer, request, err.Error(), http.StatusInternalServerError)
			return
		}
	}

	userRedisData := dto.ResponseUser{
		UserId:   userId,
		Username: dataUser.Username,
		Email:    dataUser.Email,
		Fullname: dataUser.Fullname,
		Age:      dataUser.Age,
	}

	listUser = append(listUser, userRedisData)
	err = repository.RedisSet(request.Context(), keys, listUser, 0)
	if err != nil {
		helper.HttpResponseError(writer, request, err.Error(), http.StatusInternalServerError)
		return
	}

	helper.HttpResponseSuccess(writer, request, dataUser)
}

func ListUser(writer http.ResponseWriter, request *http.Request) {
	keys := "list-user-data"
	var listUser []dto.ResponseUser
	err := repository.RedisGet(request.Context(), keys, &listUser)

	if err != nil {
		if err == redis.Nil {
			fmt.Println("get user from database")
			listUser, err = repository.GetUser()
			if err != nil {
				helper.HttpResponseError(writer, request, err.Error(), http.StatusInternalServerError)
				return
			}

			fmt.Println("set user data to redis")
			err = repository.RedisSet(request.Context(), keys, listUser, 0)
			if err != nil {
				fmt.Println(err.Error())
				helper.HttpResponseError(writer, request, err.Error(), http.StatusInternalServerError)
				return
			}
		}

		//helper.HttpResponseError(writer, request, err.Error(), http.StatusInternalServerError)
		//return
	}

	//fmt.Println(listUser)
	helper.HttpResponseSuccess(writer, request, listUser)
}

func SetRedisHM(writer http.ResponseWriter, request *http.Request) {
	var params dto.RequestJenisMakanan
	err := json.NewDecoder(request.Body).Decode(&params)
	if err != nil {
		helper.HttpResponseError(writer, request, err.Error(), http.StatusBadRequest)
		return
	}

	foodTypeMap := make(map[string]interface{})
	var dataMakanan dto.RequestJenisMakanan

	for _, value := range params.Makanan {
		if _, exist := foodTypeMap[value.TypeMakanan]; !exist {
			dataMakanan = dto.RequestJenisMakanan{}
		}
		dataMakanan.Makanan = append(dataMakanan.Makanan, value)
		dataByes, err := json.Marshal(dataMakanan)
		if err != nil {
			helper.HttpResponseError(writer, request, err.Error(), http.StatusBadRequest)
			return
		}

		foodTypeMap[value.TypeMakanan] = string(dataByes)
	}

	keysMakanan := "list-data-makanan"

	err = repository.RedisHMSet(keysMakanan, foodTypeMap, time.Minute)
	if err != nil {
		helper.HttpResponseError(writer, request, err.Error(), http.StatusBadRequest)
		return
	}

	helper.HttpResponseSuccess(writer, request, foodTypeMap)
}

func GetRedisHM(writer http.ResponseWriter, request *http.Request) {
	field := chi.URLParam(request, "field")
	keys := "list-data-makanan"
	var listMakanan dto.RequestJenisMakanan
	err := repository.RedisHMGet(keys, field, &listMakanan)
	if err != nil {
		helper.HttpResponseError(writer, request, err.Error(), http.StatusBadRequest)
		return
	}

	helper.HttpResponseSuccess(writer, request, listMakanan)

}

func GetRedisHGetAll(writer http.ResponseWriter, request *http.Request) {
	keys := "list-data-makanan"
	redisData, err := repository.RedisHGetAll(keys)
	if err != nil {
		helper.HttpResponseError(writer, request, err.Error(), http.StatusInternalServerError)
		return
	}

	responseListMakanan := make(map[string]dto.RequestJenisMakanan)
	for keyMap, valMap := range redisData {
		var dataMakanan dto.RequestJenisMakanan
		err = json.Unmarshal([]byte(valMap), &dataMakanan)
		if err != nil {
			helper.HttpResponseError(writer, request, err.Error(), http.StatusBadRequest)
			return
		}
		responseListMakanan[keyMap] = dataMakanan
	}

	helper.HttpResponseSuccess(writer, request, responseListMakanan)
}

func RedisRedisHDel(writer http.ResponseWriter, request *http.Request) {
	field := chi.URLParam(request, "field")
	keys := "list-data-makanan"

	err := repository.RedisHDel(keys, field)
	if err != nil {
		helper.HttpResponseError(writer, request, err.Error(), http.StatusInternalServerError)
		return
	}

	helper.HttpResponseSuccess(writer, request, nil)

}

func RedisRedisDel(writer http.ResponseWriter, request *http.Request) {
	keys := []string{"list-data-makanan", "list-user-data"}
	err := repository.RedisDel(keys...)
	if err != nil {
		helper.HttpResponseError(writer, request, err.Error(), http.StatusInternalServerError)
		return
	}

	helper.HttpResponseSuccess(writer, request, nil)
}
