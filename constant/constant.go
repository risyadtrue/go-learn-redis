package constant

const (
	TimeLocation    = "Asia/Jakarta"
	ContentTypeJSON = "application/json"
)
