package connection

import (
	"database/sql"
	"fmt"
	"github.com/go-redis/redis"
	_ "github.com/lib/pq"
)

const (
	host     = "localhost"
	port     = 5432
	user     = "postgres"
	password = "risyadtrue"
	dbname   = "go-redis-learn"
)

func PostgresConn() (db *sql.DB, err error) {
	// menggabunglkan string format
	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s "+
		"password=%s dbname=%s sslmode=disable",
		host, port, user, password, dbname)
	db, err = sql.Open("postgres", psqlInfo)
	return
}

func RedisConn() (client *redis.Client) {
	client = redis.NewClient(&redis.Options{
		Addr:     "127.0.0.1:6379",
		Password: "",
		DB:       0,
	})

	//pong, err := client.Ping().Result()
	//if err != nil {
	//	panic(err)
	//} else {
	//	fmt.Println(pong)
	//}

	return
}
